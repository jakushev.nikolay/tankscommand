package test

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import command.CommandException
import hwdtech.ioc.register
import hwdtech.ioc.resolve
import tank.Shot
import iobject.IObject
import iobject.Object
import iobject.ObjectGetException
import org.junit.Test
import org.junit.Assert
import org.mockito.Mockito.*
import strategy.StrategyArray
import strategy.StrategyListBullets
import tank.Move

class FireTest {

    @Test
    fun oneShell() {

        register("ListBullets", StrategyListBullets())
        register("ArrayDirections", StrategyArray())

        val position1: IntArray = intArrayOf(14, 2)
        val vecSpeed1: IntArray = intArrayOf(4, 0)
        val tanksSpawn = Object()
        tanksSpawn.set("Speed", vecSpeed1)
        tanksSpawn.set("PositionSpawn", position1)

        val mockIObject = mock<IObject>()
        `when`(mockIObject.get("Direction")).thenReturn(0)
        `when`(mockIObject.get("PositionTank")).thenReturn(3)
        `when`(mockIObject.get("Position")).thenReturn(intArrayOf(11, 2))
        `when`(mockIObject.get("Speed")).thenReturn(4)
        val moveBull = Shot(mockIObject)

        moveBull.execute()
        verify(mockIObject, times(1)).get("Direction")
        verify(mockIObject, times(1)).get("PositionTank")
        verify(mockIObject, times(1)).get("Position")
        verify(mockIObject, times(1)).get("Speed")

        val bullets = resolve<MutableList<IObject>>("ListBullets")
        Assert.assertArrayEquals(tanksSpawn.get("Speed", {}) as IntArray, bullets[0].get("Speed", {}) as IntArray)
        Assert.assertArrayEquals(
            tanksSpawn.get("PositionSpawn", {}) as IntArray,
            bullets[0].get("PositionSpawn", {}) as IntArray
        )
    }

    @Test
    fun twoShell() {

        register("ArrayDirections", StrategyArray())
        register("ListBullets", StrategyListBullets())

        val bullets = resolve<MutableList<IObject>>("ListBullets")
        val position1: IntArray = intArrayOf(14, 2)
        val vecSpeed1: IntArray = intArrayOf(4, 0)
        val tankSpawn = Object()
        tankSpawn.set("Speed", vecSpeed1)
        tankSpawn.set("PositionSpawn", position1)

        val mockIObject = mock<IObject>()
        `when`(mockIObject.get("Direction")).thenReturn(0)
        `when`(mockIObject.get("PositionTank")).thenReturn(3)
        `when`(mockIObject.get("Position")).thenReturn(intArrayOf(11, 2))
        `when`(mockIObject.get("Speed")).thenReturn(4)
        val moveBull = Shot(mockIObject)
        moveBull.execute()

        Assert.assertArrayEquals(tankSpawn.get("Speed", {}) as IntArray, bullets[0].get("Speed", {}) as IntArray)
        Assert.assertArrayEquals(
            tankSpawn.get("PositionSpawn", {}) as IntArray,
            bullets[0].get("PositionSpawn", {}) as IntArray
        )

        val bulletsTwo = resolve<MutableList<IObject>>("ListBullets")
        val mockIObject1 = mock<IObject>()
        `when`(mockIObject1.get("Direction")).thenReturn(0)
        `when`(mockIObject1.get("PositionTank")).thenReturn(3)
        `when`(mockIObject1.get("Position")).thenReturn(intArrayOf(11, 2))
        `when`(mockIObject1.get("Speed")).thenReturn(4)
        val moveBull1 = Shot(mockIObject1)
        moveBull1.execute()

        Assert.assertArrayEquals(tankSpawn.get("Speed", {}) as IntArray, bulletsTwo[1].get("Speed", {}) as IntArray)
        Assert.assertArrayEquals(
            tankSpawn.get("PositionSpawn", {}) as IntArray,
            bulletsTwo[1].get("PositionSpawn", {}) as IntArray
        )
    }

    @Test(expected = CommandException::class)
    fun cannotBeDoneSpeed() {
        val mockIObject: IObject = mock(IObject::class.java)
        `when`(mockIObject.get("Speed")).then { throw ObjectGetException() }
        `when`(mockIObject.get("Direction")).thenReturn(0)
        `when`(mockIObject.get("PositionTank")).thenReturn(3)
        `when`(mockIObject.get("Position")).thenReturn(intArrayOf(10, 2))
        val shot = Shot(mockIObject)
        shot.execute()
    }

    @Test(expected = CommandException::class)
    fun cannotBeDoneDirection() {
        val mockIObject: IObject = mock(IObject::class.java)
        `when`(mockIObject.get("Speed")).thenReturn(3)
        `when`(mockIObject.get("Direction")).then { throw ObjectGetException() }
        `when`(mockIObject.get("PositionTank")).thenReturn(3)
        `when`(mockIObject.get("Position")).thenReturn(intArrayOf(10, 2))
        val shot = Shot(mockIObject)
        shot.execute()
    }

    @Test(expected = CommandException::class)
    fun cannotBeDonePosition() {
        val mockIObject: IObject = mock(IObject::class.java)
        `when`(mockIObject.get("Speed")).thenReturn(3)
        `when`(mockIObject.get("Direction")).thenReturn(0)
        `when`(mockIObject.get("PositionTank")).thenReturn(3)
        `when`(mockIObject.get("Position")).then { throw ObjectGetException() }
        val shot = Shot(mockIObject)
        shot.execute()
    }

    @Test(expected = CommandException::class)
    fun cannotBeDonePositionTank() {
        val mockIObject: IObject = mock(IObject::class.java)
        `when`(mockIObject.get("Speed")).thenReturn(3)
        `when`(mockIObject.get("Direction")).thenReturn(0)
        `when`(mockIObject.get("PositionTank")).then { throw ObjectGetException() }
        `when`(mockIObject.get("Position")).thenReturn(10,2)
        val shot = Shot(mockIObject)
        shot.execute()
    }

    @Test(expected = CommandException::class)
    fun exceptionProperties() {
        val mockIObject: IObject = mock(IObject::class.java)
        `when`(mockIObject.get("Speed")).thenReturn(intArrayOf(10, 2))
        `when`(mockIObject.get("Position")).then { throw ObjectGetException() }
        val move = Move(mockIObject)
        move.execute()
    }

    @Test
    fun moveBullets(){
        val mockIObject: IObject = mock(IObject::class.java)
        `when`(mockIObject.get("Speed")).thenReturn(intArrayOf(10, 2))
        `when`(mockIObject.get("Position")).thenReturn(intArrayOf(4, 0))
        val move = Move(mockIObject)
        move.execute()
        verify(mockIObject, times(1)).set("Position", intArrayOf(14,2))
    }
}
