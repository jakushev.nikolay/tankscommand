package strategy

import hwdtech.ioc.IIoCResolverStrategy

class StrategyArray: IIoCResolverStrategy {

    val table:Array<DoubleArray> = arrayOf(
            doubleArrayOf(1.0,  0.0),
            doubleArrayOf(0.7,  0.7),
            doubleArrayOf(0.0,  1.0),
            doubleArrayOf(-0.7, 0.7),
            doubleArrayOf(-1.0, 0.0),
            doubleArrayOf(-0.7,-0.7),
            doubleArrayOf(0.0, -1.0),
            doubleArrayOf(0.7, -0.7))

    override fun invoke(args: Array<out Any>): Any {
        return table
    }
}
