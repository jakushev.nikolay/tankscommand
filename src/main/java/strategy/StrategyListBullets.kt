package strategy

import hwdtech.ioc.IIoCResolverStrategy

class StrategyListBullets : IIoCResolverStrategy{
    
    val list = mutableListOf<Any>()

    override fun invoke(args: Array<out Any>): Any {
        if (args.size > 0) {
            val charact = args
            list.add(charact)
        }
        val charact = args
        return list
    }
}
