package iobject

interface IObject {
    fun get(_key: String, _value: () -> Any = { throw ObjectGetException() }):Any
    fun set(_key: String, _value: Any)
}
