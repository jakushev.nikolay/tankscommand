package iobject

class Object : IObject {

    private val containerObject = hashMapOf<String, Any>()

    override fun get(_key: String, _value: () -> Any): Any {
        try {
            return  containerObject.getOrDefault(_key, _value())
        }catch(ex:Throwable) {
            throw Exception("Lambda is invalid argument")
        }
    }

    override fun set(_key: String, _value: Any) {
        containerObject.put(_key, _value)
    }
}
