package tank

import command.CommandException
import command.ICommand
import hwdtech.ioc.resolve
import iobject.IObject
import iobject.Object
import iobject.ObjectGetException
import iobject.ValueSizeException

class Shot(
    private val obj: IObject
    ): ICommand {
    override fun execute() {
        try {
            //параметры
            val direction = obj.get("Direction") as Int
            val speed = obj.get("Speed") as Int
            val positionTank = obj.get("PositionTank") as Int
            val positionBullets = obj.get("Position") as IntArray

            //
            val container = resolve<Array<DoubleArray>>("ArrayDirections")
            val speedVector = IntArray(positionBullets.size)
            val sizePosition = positionBullets.size - 1

            //сохранение по кольцевому направлению
            val directionByRing = container[directionRing(direction, container.size)]

            for (r in 0..(sizePosition)) {
                speedVector[r] =  Math.round(directionByRing[r] * speed).toInt()
                positionBullets[r] += (Math.round(directionByRing[r]) * positionTank).toInt()
            }
            createBullet(speedVector,positionBullets)
        }
        catch(e: ObjectGetException){
            throw CommandException()
        }

        catch (ex: ValueSizeException){
            throw CommandException()
        }
    }

    private fun createBullet(
        speedVec:IntArray,
        position:IntArray) {
        val listBullets = resolve<MutableList<IObject>>("ListBullets")
        val ob = Object()
        ob.set("Speed", speedVec)
        ob.set("PositionSpawn", position)
        listBullets.add(ob)
    }

    private fun directionRing(
        num:Int,
        mod:Int):Int {
        var _num=num
        while (_num<0){
            _num+=mod
        }
        return _num-mod*(_num/mod)
    }
}
