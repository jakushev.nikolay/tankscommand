package tank

import command.CommandException
import command.ICommand
import iobject.IObject
import iobject.ObjectGetException
import iobject.ValueSizeException

class Move(
    private val tank: IObject
    ) : ICommand {
    override fun execute() {
        try {
            val position = tank.get("Position") as IntArray
            val vecSpeed = tank.get("Speed") as IntArray
            val size = position.size -1

            for(i in 0..(size)){
                position[i] = position[i] + vecSpeed[i]
            }
            tank.set("Position", position)
        }
        catch(e: ObjectGetException){
            throw CommandException()
        }

        catch (ex: ValueSizeException){
            throw CommandException()
        }
    }
}
