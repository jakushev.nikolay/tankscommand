package tank

import command.ICommand

class ShotCommand(private val shot: Shot) : ICommand {
    override fun execute() {
        return shot.execute()
    }
}
